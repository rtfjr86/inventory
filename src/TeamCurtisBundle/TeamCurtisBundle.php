<?php

namespace TeamCurtisBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class TeamCurtisBundle extends Bundle
{
    public function getParent()
    {
        return 'FOSUserBundle';
    }
}
