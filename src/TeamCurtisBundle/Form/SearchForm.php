<?php

namespace TeamCurtisBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\RequestStack;

class SearchForm extends AbstractType
{

    protected $requestStack;

    /**
     * ProductType constructor.
     * @param RequestStack $requestStack
     */
    public function __construct(RequestStack $requestStack)
    {
        $this->requestStack = $requestStack;
    }

    /**
     * @inheritdoc
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('search', TextType::class, array(
                'widget_form_group_attr' => array(
                    'class' => 'col-sm-2 pull-right search-input'
                ),
                'attr' => array(
                    'class' => 'testing',
                ),
            ))
            ->add('submit', SubmitType::class, array(
                'label' => 'Search',
                'attr' => array(
                    'class' => 'btn-success pull-right',
                ),
            ))
        ;
    }
}
