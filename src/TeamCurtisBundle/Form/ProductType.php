<?php

namespace TeamCurtisBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\RequestStack;

class ProductType extends AbstractType
{

    protected $requestStack;

    /**
     * ProductType constructor.
     * @param RequestStack $requestStack
     */
    public function __construct(RequestStack $requestStack)
    {
        $this->requestStack = $requestStack;
    }

    /**
     * @inheritdoc
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $request = $this->requestStack->getCurrentRequest();
        $route = $request->get('_route');

        if ($route == 'product_create') {
            $submitLabel = 'Create';
        } elseif ($route == 'product_update') {
            $submitLabel = 'Update';
        } else {
            $submitLabel = 'Save';
        }

        $builder
            ->add('name', TextType::class)
            ->add('color', TextType::class)
            ->add('brand', TextType::class)
            ->add('category', TextType::class)
            ->add('size', TextType::class)
            ->add('quantity', IntegerType::class)
            ->add('location', TextType::class)
            ->add('submit', SubmitType::class, array(
                'label' => $submitLabel,
                'attr' => array(
                    'class' => 'btn-success',
                ),
            ))
        ;
    }
}
