<?php

namespace TeamCurtisBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use TeamCurtisBundle\Entity\User;

class LoadUserData implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $user = new User();
        $user->setUsername('admin');
        $user->setFirstName('admin');
        $user->setLastName('admin');
        $user->setEmail('admin@admin.com');
        $user->setPlainPassword('password');
        $user->setEnabled(true);
        $user->addRole('ROLE_ADMIN');
        $manager->persist($user);

        $user = new User();
        $user->setUsername('Bobby');
        $user->setFirstName('Bob');
        $user->setLastName('Dole');
        $user->setEmail('candy@gmail.com');
        $user->setPlainPassword('password');
        $user->setEnabled(true);
        $manager->persist($user);

        $user = new User();
        $user->setUsername('RickyBobby');
        $user->setFirstName('Ricky');
        $user->setLastName('Bobby');
        $user->setEmail('sweet@gmail.com');
        $user->setPlainPassword('password');
        $user->setEnabled(true);
        $manager->persist($user);

        $user = new User();
        $user->setUsername('TheSword');
        $user->setFirstName('Samurai');
        $user->setLastName('Jack');
        $user->setEmail('Aku@gmail.com');
        $user->setPlainPassword('password');
        $user->setEnabled(true);
        $manager->persist($user);

        $user = new User();
        $user->setUsername('RickMorty');
        $user->setFirstName('Rick');
        $user->setLastName('Sanchez');
        $user->setEmail('szechuan@gmail.com');
        $user->setPlainPassword('password');
        $user->setEnabled(true);
        $manager->persist($user);

        $user = new User();
        $user->setUsername('CastAway');
        $user->setFirstName('Tom');
        $user->setLastName('Hanks');
        $user->setEmail('wilson@gmail.com');
        $user->setPlainPassword('password');
        $user->setEnabled(true);
        $manager->persist($user);

        $user = new User();
        $user->setUsername('KillBill');
        $user->setFirstName('Beatrix');
        $user->setLastName('Kiddo');
        $user->setEmail('tarantino@gmail.com');
        $user->setPlainPassword('password');
        $user->setEnabled(true);
        $manager->persist($user);

        $user = new User();
        $user->setUsername('Genius');
        $user->setFirstName('Stephen');
        $user->setLastName('Hawking');
        $user->setEmail('quantum@gmail.com');
        $user->setPlainPassword('password');
        $user->setEnabled(true);
        $manager->persist($user);

        $manager->flush();
    }
}