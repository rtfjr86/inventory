<?php

namespace TeamCurtisBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use TeamCurtisBundle\Entity\Product;

class LoadProductData implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $product = new Product();
        $product->setName('KitKat');
        $product->setColor('brown');
        $product->setCategory('chocolate');
        $product->setBrand('Nestle');
        $product->setSize('small');
        $product->setQuantity(20000);
        $product->setLocation('A7');
        $manager->persist($product);

        $product = new Product();
        $product->setName('Snickers');
        $product->setColor('brown');
        $product->setCategory('chocolate');
        $product->setBrand('Mars');
        $product->setSize('small');
        $product->setQuantity(10000);
        $product->setLocation('B2');
        $manager->persist($product);

        $product = new Product();
        $product->setName('Reese\'s');
        $product->setColor('brown');
        $product->setCategory('chocolate');
        $product->setBrand('Hershey');
        $product->setSize('small');
        $product->setQuantity(30000);
        $product->setLocation('H5');
        $manager->persist($product);

        $product = new Product();
        $product->setName('Sour Patch Kids');
        $product->setColor('red');
        $product->setCategory('gummy');
        $product->setBrand('Cadbury');
        $product->setSize('large');
        $product->setQuantity(25000);
        $product->setLocation('T9');
        $manager->persist($product);

        $product = new Product();
        $product->setName('Skittles');
        $product->setColor('rainbow');
        $product->setCategory('chewy');
        $product->setBrand('Mars');
        $product->setSize('medium');
        $product->setQuantity(37000);
        $product->setLocation('A3');
        $manager->persist($product);

        $product = new Product();
        $product->setName('M&Ms');
        $product->setColor('rainbow');
        $product->setCategory('chocolate');
        $product->setBrand('Mars');
        $product->setSize('small');
        $product->setQuantity(22000);
        $product->setLocation('R2');
        $manager->persist($product);

        $product = new Product();
        $product->setName('Twizzlers');
        $product->setColor('red');
        $product->setCategory('chewy');
        $product->setBrand('Hershey');
        $product->setSize('large');
        $product->setQuantity(10000);
        $product->setLocation('D2');
        $manager->persist($product);

        $product = new Product();
        $product->setName('Gobstoppers');
        $product->setColor('rainbow');
        $product->setCategory('hard');
        $product->setBrand('Nestle');
        $product->setSize('medium');
        $product->setQuantity(30000);
        $product->setLocation('C3');
        $manager->persist($product);

        $product = new Product();
        $product->setName('Starburst');
        $product->setColor('rainbow');
        $product->setCategory('chewy');
        $product->setBrand('Wrigley');
        $product->setSize('small');
        $product->setQuantity(9001);
        $product->setLocation('D7');
        $manager->persist($product);

        $product = new Product();
        $product->setName('Gummy Bears');
        $product->setColor('rainbow');
        $product->setCategory('gummy');
        $product->setBrand('Haribo');
        $product->setSize('large');
        $product->setQuantity(17000);
        $product->setLocation('Q1');
        $manager->persist($product);

        $product = new Product();
        $product->setName('Butterscotch');
        $product->setColor('brown');
        $product->setCategory('hard');
        $product->setBrand('Werther');
        $product->setSize('medium');
        $product->setQuantity(28000);
        $product->setLocation('A6');
        $manager->persist($product);

        $product = new Product();
        $product->setName('Lemon Head');
        $product->setColor('yellow');
        $product->setCategory('hard');
        $product->setBrand('Ferrara');
        $product->setSize('small');
        $product->setQuantity(52000);
        $product->setLocation('Z7');
        $manager->persist($product);

        $product = new Product();
        $product->setName('Gummy Worms');
        $product->setColor('rainbow');
        $product->setCategory('gummy');
        $product->setBrand('Trolli');
        $product->setSize('large');
        $product->setQuantity(12000);
        $product->setLocation('E8');
        $manager->persist($product);

        $manager->flush();
    }
}