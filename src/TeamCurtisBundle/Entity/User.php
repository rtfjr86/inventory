<?php

namespace TeamCurtisBundle\Entity;

class User extends \FOS\UserBundle\Entity\User
{
    protected $firstName;
    protected $lastName;

    public function getFirstName()
    {
        return $this->firstName;
    }

    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName()
    {
        return $this->lastName;
    }

    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }
}