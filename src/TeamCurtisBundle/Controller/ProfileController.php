<?php

namespace TeamCurtisBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\DependencyInjection\ContainerAware;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use FOS\UserBundle\Model\UserInterface;
use TeamCurtisBundle\Entity\User;
use TeamCurtisBundle\TeamCurtisBundle;

class ProfileController extends Controller
{
    /**
     * Edit the user
     */
    public function editAction(Request $request)
    {
        $id = $request->query->get('id');
        $currentUser = $this->container->get('security.context')->getToken()->getUser();
        $userToEdit = $this->container->get('doctrine')->getRepository('TeamCurtisBundle:User')->find($id);

        if (!is_object($currentUser) || !$currentUser instanceof UserInterface) {
            throw new AccessDeniedException('This user does not have access to this section.');
        }

        if ($this->isGranted('ROLE_ADMIN')) {
            $user = $userToEdit;
        } else {
            $user = $currentUser;
        }

        $form = $this->container->get('fos_user.profile.form');
        $formHandler = $this->container->get('fos_user.profile.form.handler');

        $process = $formHandler->process($user);
        if ($process) {
            $this->setFlash('fos_user_success', 'profile.flash.updated');

            return new RedirectResponse($this->getRedirectionUrl($user));
        }

        return $this->container->get('templating')->renderResponse(
            'FOSUserBundle:Profile:edit.html.'.$this->container->getParameter('fos_user.template.engine'),
            array('form' => $form->createView())
        );
    }

    /**
     * Generate the redirection url when editing is completed.
     *
     * @param User $user
     * @return string
     */
    protected function getRedirectionUrl(User $user)
    {
        return $this->container->get('router')->generate('fos_user_profile_show', array('id' => $user->getId()));
    }

    /**
     * @param string $action
     * @param string $value
     */
    protected function setFlash($action, $value)
    {
        $this->container->get('session')->getFlashBag()->set($action, $value);
    }

    /**
     * Show the user
     */
    public function showAction(Request $request)
    {
        $id = $request->query->get('id');
        $currentUser = $this->container->get('security.context')->getToken()->getUser();
        $userToEdit = $this->container->get('doctrine')->getRepository('TeamCurtisBundle:User')->find($id);

        if (!is_object($currentUser) || !$currentUser instanceof UserInterface) {
            throw new AccessDeniedException('This user does not have access to this section.');
        }

        if ($this->isGranted('ROLE_ADMIN')) {
            $user = $userToEdit;
        } else {
            $user = $currentUser;
        }

        return $this->container->get('templating')->renderResponse('FOSUserBundle:Profile:show.html.'.$this->container->getParameter('fos_user.template.engine'), array('user' => $user));
    }
}