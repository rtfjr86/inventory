<?php

namespace TeamCurtisBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class UserController extends Controller
{
    public function listAction()
    {
        $users = $this->get('doctrine')->getRepository('TeamCurtisBundle:User')->findAll();

        return $this->render('TeamCurtisBundle:User:user_list.html.twig', array('users' => $users));
    }
}