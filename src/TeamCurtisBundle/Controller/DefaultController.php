<?php

namespace TeamCurtisBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use TeamCurtisBundle\Entity\Product;

class DefaultController extends Controller
{
    /**
     * Display the homepage.
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        $form = $this->createForm($this->get('form_search_type'), null, array(
            'show_legend' => false,
            'attr' => array('class' => 'clearfix'),
        ));

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            // Get variables from form and search
            $searchData = $form->get('search')->getData();

            $doctrineInstance = $this->get('doctrine');

            // $em instanceof EntityManager
            $em = $doctrineInstance->getEntityManager();

            $qb = $em->createQueryBuilder();
            $qb
                ->select('p')
                ->from(Product::class, 'p')
                ->where('p.name LIKE :searchData')
                ->orWhere('p.color LIKE :searchData')
                ->orWhere('p.brand LIKE :searchData')
                ->orWhere('p.category LIKE :searchData')
                ->orWhere('p.size LIKE :searchData')
                ->setParameter(':searchData', '%'.$searchData.'%')
                ->orderBy('p.name', 'ASC');
            ;

            $query = $qb->getQuery();
            $products = $query->getResult();

            // No search matches, get all products
            if ($products == null) {
                $this->addFlash('warning', 'No results found for data: "'.$searchData.'" .');
                $products = $this->get('doctrine')->getRepository('TeamCurtisBundle:Product')->findAll();
            } else {
                $this->addFlash('success', 'Search results for data: "'.$searchData.'" .');
            }

        // No form submitted
        } else {
            $products = $this->get('doctrine')->getRepository('TeamCurtisBundle:Product')->findAll();
        }

        return $this->render('TeamCurtisBundle:Default:index.html.twig', array(
            'products' => $products,
            'form' => $form->createView()
        ));
    }

    /**
     * Create a new product.
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function createProductAction(Request $request)
    {
        // Get the current environment.
        $env = $this->container->getParameter('kernel.environment');

        $product = new Product();
        $form = $this->createForm($this->get('form_product_type'), $product, array('label' => 'Create Product'));

        // Handle the form submission;
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // Save the new product to the database.
            $em = $this->getDoctrine()->getManager();
            $em->persist($product);
            $em->flush();

            $this->addFlash('success', 'The product "'.$product->getName().'" was successfully created.');

            return $this->redirectToRoute('team_curtis_homepage');
        }

        return $this->render('TeamCurtisBundle:Default:product_create.html.twig', array(
            'form' => $form->createView(),
            'env' => $env,
        ));
    }

    /**
     * View product details.
     *
     * @param Request $request
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function viewProductAction(Request $request, $id)
    {
        // Get the current environment.
        $env = $this->container->getParameter('kernel.environment');

        $product = $this->get('doctrine')->getRepository('TeamCurtisBundle:Product')->find($id);

        return $this->render('TeamCurtisBundle:Default:product_view.html.twig', array(
            'product' => $product,
            'env' => $env,
        ));
    }

    /**
     * Update an existing product.
     *
     * @param Request $request
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function updateProductAction(Request $request, $id)
    {
        // Get the current environment.
        $env = $this->container->getParameter('kernel.environment');

        $product = $products = $this->get('doctrine')->getRepository('TeamCurtisBundle:Product')->find($id);

        if (!$product) {
            $this->addFlash('error', 'The product you are trying to edit does not exist.');

            return new RedirectResponse($this->generateUrl('team_curtis_homepage'));
        }

        $form = $this->createForm($this->get('form_product_type'), $product, array('label' => 'Update Product'));

        // Handle the form submission;
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // Save the new product to the database.
            $em = $this->getDoctrine()->getManager();
            $em->persist($product);
            $em->flush();

            $this->addFlash('success', 'The product "'.$product->getName().'" was successfully updated.');

            return $this->redirectToRoute('product_view', array('id' => $product->getId()));
        }

        return $this->render('TeamCurtisBundle:Default:product_create.html.twig', array(
            'form' => $form->createView(),
            'env' => $env,
        ));
    }

    /**
     * Delete a product.
     *
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteProductAction($id)
    {
        if (!$this->isGranted('ROLE_ADMIN')) {
            $this->addFlash('error', 'You are not allowed to delete this product.');

            return $this->redirectToRoute('team_curtis_homepage');
        }

        $doctrine = $this->get('doctrine');
        $product = $doctrine->getRepository('TeamCurtisBundle:Product')->find($id);

        if (!$product) {
            $this->addFlash('error', 'The product you are trying to delete does not exist.');

            return new RedirectResponse($this->generateUrl('team_curtis_homepage'));
        }

        $em = $doctrine->getManager();

        $em->remove($product);
        $em->flush();

        $this->addFlash('success', 'The product "'.$product->getName().'" was successfully deleted.');

        return $this->redirectToRoute('team_curtis_homepage');
    }
}
